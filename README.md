# The way of the program (Cách thức của chương trình)
## 1.1 Chương trình là gì? 
     Là 1 chuỗi hướng dẫn chỉ định hướng dẫn thực hiện một phép  tính. Tính ttoán cũng là cái gì đó của oán, chẳng hạn như cộng trừ nhân  chia
    **input** :Lấy dữ liệu từ bàn phím, tệp, mạng hoặc một số thiết bị khác.
    **output**:Hiển thị dữ liệu trên màn hình, lưu thành tệp, gửi qua mạng, v.v.
    **math**:Thực hiện các phép toán cơ bản như cộng và nhân.
    **conditional execution**: Thực hiện các phép toán cơ bản như cộng và nhân.
    thực thi có điều kiện: Kiểm tra các điều kiện nhất định và chạy mã thích hợp

## 1.2 Running Python(Chạy Python)
       
Một trong những thách thức khi bắt đầu với Python là bạn có thể phải cài đặtPython và phần mềm liên quan trên máy tính của mình. Nếu bạn đã quen với hệ điều hành của mình và đặc biệt nếu bạn cảm thấy thoải mái với giao diện dòng lệnh, bạn sẽ không gặp khó khăn khi cài đặt Python. Nhưng đối với người mới bắt đầu, có thể khó khăn khi học về quản trị hệ thống và lập trình cùng một lúc.
Để tránh vấn đề đó, tôi khuyên bạn nên bắt đầu chạy Python trong trình duyệt. Sau này, khi bạn đã quen với Python, tôi sẽ đưa ra đề xuất để cài đặt Python trên máy tính của bạn.
Tôi cung cấp các hướng dẫn chi tiết để bắt đầu (http: //tinyurl.com/thinkpython2e.)
Có hai phiên bản Python, được gọi là Python 2 và Python 3. Chúng rất giống nhau, nếu bạn học một cái thì rất dễ chuyển sang cái kia. Trên thực tế, chỉ có một số khác biệt mà bạn sẽ gặp phải khi mới bắt đầu. Cuốn sách này được viết cho Python 3, nhưng tôi bao gồm một số ghi chú về Python 2.
## 1.3The first program(Chương trình đầu tiên)
Theo truyền thống, chương trình đầu tiên bạn viết bằng ngôn ngữ mới được gọi là “Hello, World!” bởi vì tất cả những gì nó làm là hiển thị dòng chữ “Xin chào, Thế giới!”. Trong Python, nó trông giống như sau: 
>>> print ('Hello, World!')
  Đây là một ví dụ về câu lệnh in, mặc dù nó không thực sự in bất kỳ thứ gì trên giấy. Nó hiển thị một kết quả trên màn hình.
  ## 1.4    Arithmetic operators(Toán tử số học)
  Sau “Hello, World”, bước tiếp theo là số học. Python cung cấp các toán tử, là các ký hiệu đặc biệt đại diện cho các phép tính như cộng và nhân.
>>            40            +            242
>>>            43            -            142
>>>            6            *            742
Toán tử / thực hiện phép chia:
>>            84            /            242.0
## 1.5Values and types(Giá trị và loại)
- Giá trị là một trong những thứ cơ bản mà chương trình làm việc, như một chữ cái hoặc một số. Một số giá trị mà chúng tôi đã thấy cho đến nay là 2,42,0 và Hello, World! '.
- Các giá trị này thuộc về các kiểu khác nhau: 2 là số nguyên, 42.0 là số điểm nổi và 'Xin chào, Thế giới!' Là dấu gạch chéo, được gọi như vậy vì các chữ cái trong nó được xâu chuỗi lại với nhau. Nếu bạn không chắc loại giá trị có , thông dịch viên có thể cho bạn biết:
>>            type(2)<class'int'>
>>>            type(42.0)<class'float'>
>>>            type('Hello,            World!')
## 1.6 Formal and natural languages ( Ngôn ngữ trang trọng và tự nhiên)
-Ngôn ngữ  tự nhiên **(Natural languagesare)** là những ngôn ngữ mà mọi người nói, chẳng hạn như tiếng Anh, tiếng Tây Ban Nha và tiếng Pháp, chúng không được thiết kế bởi con người (mặc dù con người cố gắng áp đặt một số trật tự cho chúng); chúng phát triển một cách tự nhiên.
-Ngôn ngữ chính thức **Formal languagesare** à ngôn ngữ được thiết kế bởi mọi người cho các ứng dụng cụ thể. Ví dụ, ký hiệu mà các nhà toán học sử dụng là một ngôn ngữ chính thức đặc biệt tốt trong việc biểu thị mối quan hệ giữa các con số và ký hiệu.
## 1.7 Debugging(Gỡ lỗi)
-Lập trình viên mắc lỗi. Vì những lý do hay thay đổi, các lỗi lập trình được gọi là lỗi và quá trình theo dõi chúng được gọi là gỡ lỗi. Lập trình, và đặc biệt là gỡ lỗi, đôi khi mang lại cảm xúc mạnh mẽ
## 1.8 Glossary(Ngành kinh doanh)
-giải quyết vấn đề: Quá trình hình thành một vấn đề, tìm ra giải pháp và thể hiện nó.
-ngôn ngữ cấp cao: Một ngôn ngữ lập trình như Python được thiết kế để người dùng dễ đọc và viết.
n-gôn ngữ cấp thấp: Một ngôn ngữ lập trình được thiết kế để máy tính chạy dễ dàng; còn được gọi là “ngôn ngữ máy” hoặc “hợp ngữ”.
-tính di động: Thuộc tính của một chương trình có thể chạy trên nhiều loại máy tính.
-thông dịch viên: Một chương trình đọc một chương trình khác và thực thi nó
-nhắc: Các ký tự được trình thông dịch hiển thị để cho biết rằng nó đã sẵn sàng để nhận đầu vào từ người dùng.
-chương trình: Một tập hợp các hướng dẫn chỉ định một phép tính.
-câu lệnh in: Một lệnh khiến trình thông dịch Python hiển thị một giá trị trên màn hình.
-toán tử: Một ký hiệu đặc biệt đại diện cho một phép tính đơn giản như cộng, nhân-cation hoặc nối chuỗi.
-value: Một trong những đơn vị dữ liệu cơ bản, như số hoặc chuỗi, mà chương trình thao tác.
-type: Một loại giá trị. Các kiểu mà chúng ta đã thấy cho đến nay là số nguyên (typeint), số dấu phẩy động (typefloat) và chuỗi (typestr).
-số nguyên: Một kiểu biểu thị các số nguyên.
-dấu phẩy động: Một kiểu biểu thị số có phần thập phân.
-string: Kiểu biểu diễn chuỗi ký tự.
-ngôn ngữ tự nhiên: Bất kỳ một trong những ngôn ngữ mà mọi người nói được phát triển tự nhiên.
-ngôn ngữ chính thức: Bất kỳ một trong những ngôn ngữ mà mọi người đã thiết kế cho các tư thế cụ thể, chẳng hạn như đại diện cho các ý tưởng toán học hoặc chương trình máy tính; tất cả các ngôn ngữ ghép chương trình đều là ngôn ngữ chính thức.
-token: Một trong những yếu tố cơ bản của cấu trúc cú pháp của chương trình, tương tự như aword trong ngôn ngữ tự nhiên.
-cú pháp: Các quy tắc chi phối cấu trúc của một chương trình.
-parse: Để kiểm tra một chương trình và phân tích cấu trúc cú pháp.
-bug: Lỗi trong một chương trình.
-gỡ lỗi: Quá trình tìm kiếm và sửa lỗi.
# Variables, expressions andstatements

## 2.1Assignment statements(Các câu lệnh gán)
-Tạo ra cho nó một biến mới và gán cho nó một gái trị

>>>            n            =            17
>>>            pi            =            3.1415926535897932
## 2.2Variable names (Tên biến)
-Tên biến có thể dài tùy thích,chứa cả chữ và số nhưng khống bắt đầu bằng số .Thường sử dụng chữ thường cho tên biến, ký  tự gạch dưới cũng có thể xuất hiện trong tên biến
-Nếu bạn đặt tên không hợp lệ cho một biến, bạn sẽ gặp lỗi cú pháp
>>>            76trombones            ='big parade'
>>>            more@            =            1000000
>>>            class            ='Advanced Theoretical Zymurgy'
-Một số từ không thể dùng làm tên biến :
>>>False ,class,  finally ,is, return, None ,continue ,for  ....    
## 2.3    Expressions and statements(Biểu thức và tuyên bố)
-Một biểu thức là sự kết hợp của các giá trị, biến và toán tử. Bản thân một giá trị được coi là một biểu thức và một biến số cũng vậy, vì vậy tất cả các biểu thức sau đều là biểu thức hợp pháp:
>>>            42
>>> n=17
>>> n + 2542
-Câu lệnh là một đơn vị mã có tác dụng, như tạo một biến hoặc hiển thị giá trị:
>>            n=17
>>>            print(n)
## 2.4    Script mode(chế đọ tập lệnh)
## 2.5 Order of operations( Trình tự hoạt động)  
-Khi một biểu thức chứa nhiều hơn một toán tử, thứ tự đánh giá phụ thuộc vào thứ tự hoạt động. Đối với các toán tử toán học, Python tuân theo sự phát minh toán học. Từ viết tắt PEMDAS là một cách hữu ích để ghi nhớ các quy tắc:
• Dấu ngoặc đơn **Parentheses** có mức độ ưu tiên cao nhất và có thể được sử dụng để buộc một biểu thức đánh giá theo thứ tự bạn muốn
>>>            2*(3-1)= 4,
• Luỹ thừa **Exponentiation** có mức độ ưu tiên cao nhất tiếp theo
• Phép nhân **Multiplication** và phép chia **Division** có mức độ ưu tiên cao hơn phép cộng và phép trừ.
• Các toán tử **Operators** có cùng thứ tự ưu tiên được đánh giá từ trái sang phải (ngoại trừ lũy thừa).
## 2.6 String operations (Các phép toán chuỗi) 
-Thực hiện theo quy luật có sẵn . Nhưng có 2 ngoại lệ đó là + và *   
##  2.7    Comments(Nhận xét)
-Cách nhanh để tìm ra lỗi và giải quyết nó là chú thích bằng cách  ghi chú vào chương trình 
Kí hiệu:#
## 2.8 Debugging(Gỡ lỗi) 
-Syntax error (lỗ cú pháp) đề cập đến cấu trúc chương trình và các quy tắc có sẵn 
>>> một câu lệnh sai => ko thể chạy chương trình
-Runtime error (lỗi thời gian) ko xuất hiện cho  đến khi chương trình chạy
-Sematic error (lỗi ngữ nghĩa )chương trình vẫn chạy nhưng không cho ra kết quả như mong muốn
##  2.9 Glossary(Chú Thích Thuật ngữ )
- variable(biến)  tên  tham am chiếu đến một ột giá trị
-  assignment(gán)câu lệnh gán cho một biến
- state diagram (sơ đò trạng thái ) mmotj biểu diến đồ hạo cho một tập hợp và các giá trị tham chiếu đến chúng
- key work từ dành riêng được sử dụng để pt cú pháp
- opera (toán hạng) 1 trong toán tử hoạt động
- exprission (biểu thức) sự kết hợp giữa các biến
- evaluate (đánh giá) để mang lại một giá trị duy nhất
-statement (câu lệnh ) mã đại diện cho một câu lệnh hay một hành động

